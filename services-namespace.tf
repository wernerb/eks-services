locals {
  services_namespace = var.create_services_namespace ? join("", kubernetes_namespace.services.*.metadata[0].name) : var.services_namespace
}

resource "kubernetes_namespace" "services" {
  count = var.create_services_namespace ? 1 : 0

  metadata {
    name = var.services_namespace
  }
}
