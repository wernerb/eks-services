locals {
  external_dns_name           = "external-dns"
  external_dns_namespace      = local.services_namespace
  external_dns_helm_repo_name = "stable"

  external_dns_helm_values = var.install_external_dns ? templatefile("${path.module}/files/templates/external-dns-helm-values.yaml.tpl", {
    full_name = local.external_dns_name

    cluster_name = var.cluster_name

    aws_region        = var.aws_region
    route53_zone_type = "public"

    domain_filters = yamlencode({
      domainFilters = var.external_dns_domain_filters
    })

    dns_record_policy = var.external_dns_record_policy

    service_account_name                = local.external_dns_name
    service_account_iam_role_annotation = "${local.eks_iam_service_account_annotation}: ${module.external_dns_iam_role.this_iam_role_arn}"
  }) : ""

  external_dns_iam_resource_name = "k8s-${local.external_dns_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

resource "helm_release" "external_dns" {
  count = var.install_external_dns ? 1 : 0

  name      = local.external_dns_name
  chart     = "${local.external_dns_helm_repo_name}/${local.external_dns_name}"
  namespace = local.external_dns_namespace
  version   = var.external_dns_helm_chart_version

  values = concat([local.external_dns_helm_values], var.external_dns_helm_values)

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = false
    for_each = var.external_dns_integration_isio ? [1] : []

    content {
      name  = "sources"
      value = "{service,ingress,istio-gateway}"
    }
  }

  depends_on = [null_resource.wait_for_tiller]
}

# Create IAM role with required policies for external dns pod
module "external_dns_iam_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 2.22"

  create_role = var.install_external_dns

  role_name = local.external_dns_iam_resource_name

  provider_url     = var.oidc_provider_url
  role_policy_arns = [join("", aws_iam_policy.external_dns.*.arn)]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.external_dns_namespace}:${local.external_dns_name}"]

  tags = var.tags
}

resource "aws_iam_policy" "external_dns" {
  count = var.install_external_dns ? 1 : 0

  name   = local.external_dns_iam_resource_name
  policy = join("", data.aws_iam_policy_document.external_dns_role_policy.*.json)
}

data "aws_iam_policy_document" "external_dns_role_policy" {
  count = var.install_external_dns ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "route53:ListTagsForResources",
      "route53:GetHostedZone",
      "route53:ListTrafficPolicyInstancesByHostedZone",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
      "route53:ListVPCAssociationAuthorizations",
      "route53:ListTagsForResource",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "route53:ListTrafficPolicyInstances",
      "route53:ListHostedZones",
      "route53:ListTrafficPolicies",
      "route53:TestDNSAnswer",
      "route53:ListHostedZonesByName",
    ]

    resources = ["*"]
  }
}
