locals {
  sb_aws_configure_custom_templates = var.install_service_broker_aws && var.service_broker_aws_custom_templates_enabled

  create_custom_templates_bucket           = local.sb_aws_configure_custom_templates && var.service_broker_aws_custom_templates_bucket_name == ""
  use_existing_bucket_for_custom_templates = local.sb_aws_configure_custom_templates && var.service_broker_aws_custom_templates_bucket_name != ""

  custom_templates_bucket_name   = local.create_custom_templates_bucket ? join("", aws_s3_bucket.aws_sb_custom_templates_created.*.bucket) : var.service_broker_aws_custom_templates_bucket_name
  custom_templates_bucket_arn    = local.create_custom_templates_bucket ? join("", aws_s3_bucket.aws_sb_custom_templates_created.*.arn) : format("arn:aws:s3:::%s", var.service_broker_aws_custom_templates_bucket_name)
  custom_templates_bucket_region = local.create_custom_templates_bucket ? join("", aws_s3_bucket.aws_sb_custom_templates_created.*.region) : var.service_broker_aws_custom_templates_bucket_region

  custom_templates_path = var.service_broker_aws_custom_templates_path != "" ? var.service_broker_aws_custom_templates_path : "${path.module}/files/aws-service-broker/custom-templates"
  custom_templates      = formatlist("%s/%s", local.custom_templates_path, fileset(local.custom_templates_path, "*"))
}

resource "aws_s3_bucket" "aws_sb_custom_templates_created" {
  count = local.create_custom_templates_bucket ? 1 : 0

  bucket = "aws-sb-templates-${var.aws_region}-${var.environment}-${lower(var.cluster_name)}"
  acl    = "private"

  tags = merge(var.tags, {
    Name = "AWS Service Broker Templates"
  })
}

resource "aws_s3_bucket_object" "custom_template" {
  count = local.sb_aws_configure_custom_templates ? length(local.custom_templates) : 0

  bucket = local.custom_templates_bucket_name
  key    = "${var.service_broker_aws_custom_templates_bucket_prefix}/${basename(local.custom_templates[count.index])}"
  source = local.custom_templates[count.index]
  etag   = md5("${var.service_broker_aws_custom_templates_bucket_prefix}/${basename(local.custom_templates[count.index])}")

  tags = var.tags
}
