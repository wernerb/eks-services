locals {
  metrics_server_helm_values = var.install_metrics_server ? templatefile("${path.module}/files/templates/metrics-server-helm-values.yaml.tpl", {}) : ""
}

resource "helm_release" "metrics_server" {
  count = var.install_metrics_server ? 1 : 0

  name      = "metrics-server"
  chart     = "stable/metrics-server"
  namespace = local.services_namespace
  version   = var.metrics_server_helm_chart_version

  values = concat([local.metrics_server_helm_values], var.metrics_server_helm_values)

  depends_on = [null_resource.wait_for_tiller]
}
