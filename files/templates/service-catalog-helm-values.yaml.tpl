controllerManager:
  replicas: ${replicas}
  # healthcheck configures the readiness and liveliness probes for the controllerManager pod.
  healthCheck:
    enabled: ${health_check_enabled}
  # Controller Manager resource requests and limits
  # Ref: http://kubernetes.io/docs/user-guide/compute-resources/
  resources: ${resources}

rbacEnable: true

asyncBindingOperationsEnabled: ${async_binding_enabled}
