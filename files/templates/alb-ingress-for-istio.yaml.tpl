apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ${name}
  namespace: ${namespace}
  annotations:
    kubernetes.io/ingress.class: alb
    alb.ingress.kubernetes.io/actions.ssl-redirect: '{"Type": "redirect", "RedirectConfig": { "Protocol": "HTTPS", "Port": "443", "StatusCode": "HTTP_301"}}'
    alb.ingress.kubernetes.io/backend-protocol: '${backend_protocol}'
    alb.ingress.kubernetes.io/certificate-arn: '${tls_certificate_arn}'
    alb.ingress.kubernetes.io/listen-ports: '[ {"HTTPS":443}, {"HTTP":80} ]'
    alb.ingress.kubernetes.io/healthcheck-port: '15020'
    alb.ingress.kubernetes.io/scheme: internet-facing
    alb.ingress.kubernetes.io/ssl-policy: ${ssl_policy}
    alb.ingress.kubernetes.io/subnets: ${subnet_ids}
    alb.ingress.kubernetes.io/security-groups: "${security_group_ids}"
    alb.ingress.kubernetes.io/success-codes: 200,404
    alb.ingress.kubernetes.io/tags: ${tags}
    alb.ingress.kubernetes.io/target-type: ${target_type}
    alb.ingress.kubernetes.io/load-balancer-attributes: routing.http2.enabled=true,access_logs.s3.enabled=${logs_s3_enabled}%{ if logs_s3_enabled },access_logs.s3.bucket=${logs_s3_bucket},access_logs.s3.prefix=${logs_s3_prefix}%{ endif }
    %{ if waf_enabled && waf_acl_arn != "" }alb.ingress.kubernetes.io/wafv2-acl-arn: ${waf_acl_arn}%{ endif }
spec:
  rules:
  - host: '*.${host_name}'
    http:
      paths:
      - backend:
          serviceName: ssl-redirect
          servicePort: use-annotation
      - backend:
          serviceName: istio-ingressgateway
          servicePort: %{ if backend_protocol == "HTTPS"}443%{ else }80%{ endif }
