variable "eks_cluster_name" {}

variable "oidc_provider_url" {}

variable "aws_region" {
  default = "eu-west-1"
}

provider "aws" {
  region = var.aws_region
}

data "aws_caller_identity" "current" {}

module "default" {
  source = "../.."

  project     = "proj"
  environment = "env"

  aws_region     = var.aws_region
  aws_account_id = data.aws_caller_identity.current.account_id

  cluster_name      = var.eks_cluster_name
  oidc_provider_url = var.oidc_provider_url
}
