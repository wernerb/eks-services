locals {
  aws_lb_name      = "aws-load-balancer"
  aws_lb_namespace = local.services_namespace

  aws_lb_helm_values = templatefile("${path.module}/files/templates/aws-lb-helm-values.yaml.tpl", {
    name_override = local.aws_lb_name
    cluster_name  = var.cluster_name
    region        = var.aws_region
    vpc_id        = var.vpc_id

    container_image_tag = var.aws_lb_container_image_tag

    service_account_name = local.aws_lb_name

    service_account_iam_role_annotation = jsonencode({
      (local.eks_iam_service_account_annotation) = module.aws_lb_iam_role.this_iam_role_arn
    })

    resources = jsonencode(var.aws_lb_resources)
  })

  aws_lb_iam_resource_name = "k8s-${local.aws_lb_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

##
# Source: https://github.com/aws/eks-charts/blob/master/stable/aws-load-balancer-controller/crds/crds.yaml
##
module "aws_lb_controller_crd_apply" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply?ref=v1.0.11"

  enabled = var.install_aws_lb

  manifest   = file("${path.module}/files/manifests/aws-lb-crds.yaml")
  kubeconfig = local.kubeconfig_file

  silence_apply_errors = false # break terraform apply if apply fails

  enable_delete_on_destroy = true # un-install on destroy
  silence_delete_errors    = true # don't break terraform destroy if k8s in not responding well to delete
}

resource "helm_release" "aws_lb" {
  count = var.install_aws_lb ? 1 : 0

  name       = local.aws_lb_name
  namespace  = local.aws_lb_namespace
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  version    = var.aws_lb_helm_chart_version

  values = concat([local.aws_lb_helm_values], var.aws_lb_helm_values)

  depends_on = [
    null_resource.wait_for_tiller,
  ]
}

# Create IAM role with required policies for AWS ALB Load Balancer Controller
module "aws_lb_iam_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 2.22"

  create_role = var.install_aws_lb

  role_name = local.aws_lb_iam_resource_name

  provider_url     = var.oidc_provider_url
  role_policy_arns = [join("", aws_iam_policy.aws_lb.*.arn)]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.aws_lb_namespace}:${local.aws_lb_name}"]

  tags = var.tags
}

resource "aws_iam_policy" "aws_lb" {
  count = var.install_aws_lb ? 1 : 0

  name   = local.aws_lb_iam_resource_name
  policy = join("", data.aws_iam_policy_document.aws_lb_iam_role_policy.*.json)

}

# Source: https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/main/docs/install/iam_policy.json
data "aws_iam_policy_document" "aws_lb_iam_role_policy" {
  count = var.install_aws_lb ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "acm:DescribeCertificate",
      "acm:ListCertificates",
      "acm:GetCertificate",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateSecurityGroup",
      "ec2:CreateTags",
      "ec2:DeleteTags",
      "ec2:DeleteSecurityGroup",
      "ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses",
      "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus",
      "ec2:DescribeInternetGateways",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeTags",
      "ec2:DescribeVpcs",
      "ec2:ModifyInstanceAttribute",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:RevokeSecurityGroupIngress",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "elasticloadbalancing:AddListenerCertificates",
      "elasticloadbalancing:AddTags",
      "elasticloadbalancing:CreateListener",
      "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateRule",
      "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener",
      "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteRule",
      "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:DescribeListenerCertificates",
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DescribeRules",
      "elasticloadbalancing:DescribeSSLPolicies",
      "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetGroupAttributes",
      "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:ModifyRule",
      "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:RemoveTags",
      "elasticloadbalancing:SetIpAddressType",
      "elasticloadbalancing:SetSecurityGroups",
      "elasticloadbalancing:SetSubnets",
      "elasticloadbalancing:SetWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "iam:CreateServiceLinkedRole",
      "iam:GetServerCertificate",
      "iam:ListServerCertificates",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "cognito-idp:DescribeUserPoolClient",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "waf-regional:GetWebACLForResource",
      "waf-regional:GetWebACL",
      "waf-regional:AssociateWebACL",
      "waf-regional:DisassociateWebACL"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "tag:GetResources",
      "tag:TagResources",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "waf:GetWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "wafv2:GetWebACL",
      "wafv2:GetWebACLForResource",
      "wafv2:AssociateWebACL",
      "wafv2:DisassociateWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "shield:DescribeProtection",
      "shield:GetSubscriptionState",
      "shield:DeleteProtection",
      "shield:CreateProtection",
      "shield:DescribeSubscription",
      "shield:ListProtections"
    ]

    resources = ["*"]
  }
}

resource "local_file" "aws_lb_helm_release_values" {
  count = var.config_output_enabled && var.install_aws_lb ? 1 : 0

  filename = "${var.config_output_path}/${local.aws_lb_name}-helm-values.yaml"
  content  = local.aws_lb_helm_values
}
