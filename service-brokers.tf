# Open Service Brokers API https://www.openservicebrokerapi.org/
#
# This file creates a namespace and any otehr relevant resources to support
# the operation of a Service Catalog and Service Brokers implementing Open Service Broker API.
#
# Brokers we use:
# - AWS Service Broker
locals {
  service_brokers_namespace = "open-service-brokers"
}

resource "kubernetes_namespace" "open_service_brokers" {
  count = local.install_service_catalog ? 1 : 0

  metadata {
    name = local.service_brokers_namespace
  }
}
