locals {
  service_broker_name = "aws-sb"

  service_broker_aws_helm_values = templatefile("${path.module}/files/templates/service-broker-aws-helm-values.yaml.tpl", {
    container_image_with_tag = var.service_broker_aws_container_image_with_tag

    aws_region          = var.aws_region
    dynamodb_table_name = join("", aws_dynamodb_table.service_broker_aws.*.id)

    broker_id = local.service_broker_name
  })

  service_broker_iam_resource_name = "k8s-${local.service_broker_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"

  service_broker_aws_dynamodb_table = "${local.service_broker_name}-${var.project}-${var.environment}-${var.aws_region}"

  service_broker_aws_irsa_triggers = {
    chart    = join("", helm_release.service_broker_aws.*.metadata.0.chart)
    revision = join("", helm_release.service_broker_aws.*.metadata.0.revision)
    version  = join("", helm_release.service_broker_aws.*.metadata.0.version),
    values   = sha1(join("", helm_release.service_broker_aws.*.metadata.0.values)),
  }
}

resource "helm_release" "service_broker_aws" {
  count = var.install_service_broker_aws ? 1 : 0

  name       = local.service_broker_name
  namespace  = local.service_brokers_namespace
  repository = var.service_broker_helm_repo_url
  chart      = "aws-servicebroker"
  version    = var.service_broker_aws_helm_chart_version

  wait = var.service_broker_aws_helm_wait

  values = concat([local.service_broker_aws_helm_values], var.service_broker_aws_helm_values)

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = false
    for_each = local.sb_aws_configure_custom_templates ? [1] : []

    content {
      name  = "aws.bucket"
      value = local.custom_templates_bucket_name
    }
  }

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = fals
    for_each = local.sb_aws_configure_custom_templates ? [1] : []

    content {
      name  = "aws.key"
      value = var.service_broker_aws_custom_templates_bucket_prefix
    }
  }

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = fals
    for_each = local.sb_aws_configure_custom_templates ? [1] : []

    content {
      name  = "aws.s3region"
      value = local.custom_templates_bucket_region
    }
  }

  depends_on = [
    null_resource.wait_for_tiller,
    helm_release.service_catalog,
  ]
}

#########################################################
# IAM POlicy for AWS Service Broker
#########################################################
# When we upgrade de broker the SA is recreated and the IAM OIDC annotation is lost.
# This module will trigger when the upgrade happens and it will re-annotate the SA.
module "service_broker_aws_iam_role" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/iam-role-for-k8s-service-account?ref=v1.0.16"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  create            = var.install_service_broker_aws
  create_iam_role   = true
  external_triggers = local.service_broker_aws_irsa_triggers

  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region

  kubeconfig_file           = local.kubeconfig_file
  cluster_oidc_provider_url = var.oidc_provider_url

  aws_iam_role_name = local.service_broker_iam_resource_name

  k8s_service_account_namespace = local.service_brokers_namespace
  k8s_service_account_name      = "${local.service_broker_name}-aws-servicebroker-service"
  k8s_fully_defined_resources   = ["deployment/${local.service_broker_name}-aws-servicebroker"]
  aws_iam_role_policy_arns      = [join("", aws_iam_policy.service_broker_aws.*.arn)]
}

resource "aws_iam_policy" "service_broker_aws" {
  count = var.install_service_broker_aws ? 1 : 0

  name   = local.service_broker_iam_resource_name
  policy = join("", data.aws_iam_policy_document.service_broker_aws.*.json)
}

data "aws_iam_policy_document" "service_broker_aws" {
  count = var.install_service_broker_aws ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = compact([
      "arn:aws:s3:::awsservicebroker",
      var.service_broker_aws_custom_templates_enabled ? local.custom_templates_bucket_arn : ""
    ])
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject"
    ]

    resources = compact([
      "arn:aws:s3:::awsservicebroker/templates/*",
      var.service_broker_aws_custom_templates_enabled ? "${local.custom_templates_bucket_arn}/*" : ""
    ])
  }

  statement {
    effect = "Allow"

    actions = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
    ]

    resources = [
      join("", aws_dynamodb_table.service_broker_aws.*.arn)
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters",
    ]

    resources = [
      "arn:aws:ssm:${var.aws_region}:${var.aws_account_id}:parameter/asb-*",
      "arn:aws:ssm:${var.aws_region}:${var.aws_account_id}:parameter/Asb*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:PutParameter",
    ]

    resources = [
      "arn:aws:ssm:${var.aws_region}:${var.aws_account_id}:parameter/asb-*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "cloudformation:CreateStack",
      "cloudformation:DeleteStack",
      "cloudformation:DescribeStacks",
      "cloudformation:UpdateStack",
      "cloudformation:CancelUpdateStack",
      "cloudformation:DescribeStackEvents",
    ]

    resources = [
      "arn:aws:cloudformation:${var.aws_region}:${var.aws_account_id}:stack/aws-service-broker-*/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "athena:*",
      "dynamodb:*",
      "kms:*",
      "elasticache:*",
      "elasticmapreduce:*",
      "kinesis:*",
      "rds:*",
      "redshift:*",
      "route53:*",
      "s3:*",
      "sns:*",
      "sqs:*",
      "ec2:*",
      "iam:*",
      "lambda:*",
    ]

    resources = [
      "*",
    ]
  }
}

#########################################################
# AWS Service Broker uses DynamoDB
#########################################################
resource "aws_dynamodb_table" "service_broker_aws" {
  count = var.install_service_broker_aws ? 1 : 0

  name           = local.service_broker_aws_dynamodb_table
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "id"
  range_key      = "userid"

  attribute {
    name = "userid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "type"
    type = "S"
  }

  global_secondary_index {
    name               = "type-userid-index"
    hash_key           = "type"
    range_key          = "userid"
    write_capacity     = 5
    read_capacity      = 5
    projection_type    = "INCLUDE"
    non_key_attributes = ["id", "locked"]
  }

  server_side_encryption {
    enabled = var.enable_kms_encyption
  }

  tags = merge(var.tags, {
    "Name"    = local.service_broker_aws_dynamodb_table
    "cluster" = var.cluster_name
  })
}
